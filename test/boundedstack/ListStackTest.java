package boundedstack;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ListStackTest {
    private Stack<String> empty3; // empty stack with capacity of 3
    private Stack<String> abc6; // stack with elements A, B, and C with capacity of 6

    @Before
    public void setUp() throws Exception {
        empty3 = new ListStack<>(3);
        abc6 = new ListStack<>(6);
        abc6.push("A");
        abc6.push("B");
        abc6.push("C");
    }

    @Test
    public void setup() {
        Assert.assertEquals(empty3.depth(), 0);
        Assert.assertEquals(empty3.capacity(), 3);
        Assert.assertEquals(abc6.depth(), 3);
        Assert.assertEquals(abc6.capacity(), 6);
    }

    @Test(expected = IllegalStateException.class)
    public void test_illegal_state_exception_is_full() throws IllegalStateException {
        empty3.push("First");
        empty3.push("Second");
        empty3.push("Third");
        empty3.push("Fourth");
    }

    // Note: The push method is tested by the setup test.

    @Test
    public void pop() {
        abc6.pop();
        Assert.assertEquals(abc6.depth(), 2);
        Assert.assertEquals(abc6.capacity(), 6);
    }

    @Test(expected = IllegalStateException.class)
    public void test_illegal_state_exception_is_empty() throws IllegalStateException {
        empty3.pop();
        empty3.pop();
        empty3.pop();
        empty3.pop();
    }

    // Note: depth and capacity are simple getters that are used to test other methods.
}