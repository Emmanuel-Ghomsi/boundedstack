package boundedstack;

import java.util.ArrayList;
import java.util.List;

public class ListStack<E> implements Stack<E> {
    private List<E> contents;
    private final int capacity;

    public ListStack(int capacity) {
        this.capacity = capacity;
        this.contents = new ArrayList<>();
    }

    public boolean isFull() {
        return depth() == capacity();
    }

    public boolean isEmpty() {
        return depth() == 0;
    }

    /**
     * Adds the specified element to the top of this ListStack
     * @param element the element to be added to this ListStack
     * @throws IllegalStateException if this ListStack is full
     */
    @Override
    public void push(E element) {
        if (isFull())
            throw new IllegalStateException();
        contents.add(depth(), element);
    }

    /**
     * Removes and returns an element from the top of this ListStack
     *
     * @return the element removed from this ListStack
     * @throws IllegalStateException if this ListStack is empty
     */
    @Override
    public E pop() {
        if (isEmpty())
            throw new IllegalStateException();
        return contents.remove(depth() - 1);
    }

    /**
     * Returns the depth of this ListStack
     * @return the number of elements currently in this ListStack
     */
    @Override
    public int depth() {
        return this.contents.size();
    }

    /**
     * Returns the capacity of this ListStack
     * @return the maximum number of elements this ListStack can hold
     */
    @Override
    public int capacity() {
        return this.capacity;
    }
}
